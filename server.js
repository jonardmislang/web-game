const express = require('express');
const axios = require('axios');
const path = require('path');

const app = express();
const PORT = process.env.PORT || 3003;

// Serve static files from the 'public' directory
app.use(express.static('public'));

// Handle GET requests to the root URL
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '', 'index.html'));
});


// Latency testing endpoint
app.get('/test-latency', async (req, res) => {
    const { url, count } = req.query;
    const latencies = [];

    try {
        for (let i = 0; i < count; i++) {
            const start = Date.now();
            await axios.get(url); // Send a GET request
            const latency = Date.now() - start;
            latencies.push(latency);
        }

        const totalLatency = latencies.reduce((a, b) => a + b, 0);

        res.json({
            totalLatency: totalLatency,
            latencies: latencies
        });
    } catch (error) {
        console.error('Error fetching the URL:', error);
        res.status(500).json({ error: 'Failed to fetch the URL' });
    }
});

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});