// Part 1: Initialization and Event Listeners
document.addEventListener("DOMContentLoaded", function() {
    const testButton = document.getElementById("testButton");

    testButton.addEventListener("click", function() {
        const userCount = parseInt(document.getElementById("userCount").value);
        const requestCount = parseInt(document.getElementById("requestCount").value);
        document.getElementById("userCountError").innerHTML = "";
        document.getElementById("requestCountError").innerHTML = "";

        let hasError = false;
        if (userCount < 1 || userCount > 499) {
            document.getElementById("userCountError").innerHTML = "Number of Users must be between 1 and 499.";
            hasError = true;
        }
        if (requestCount < 1 || requestCount > 10) {
            document.getElementById("requestCountError").innerHTML = "Requests per User must be between 1 and 10.";
            hasError = true;
        }

        if (!hasError) {
            console.log("Valid inputs. Proceeding with test...");
            performLatencyTest(userCount, requestCount);
        }
    });
});

// Part 2: Latency Testing
async function performLatencyTest(userCount, requestCount) {
    const apiUrl = document.getElementById('apiUrl').value;
    const websiteUrl = document.getElementById('websiteUrl').value;
    const url = apiUrl || websiteUrl;

    if (!url) {
        alert('Please enter a valid API URL or Website URL.');
        return;
    }

    document.getElementById('loader').style.display = 'block';
    document.getElementById('result').innerHTML = '';
    const ctx = document.getElementById('latencyChart').getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    try {
        const response = await fetch(`test-latency?url=${encodeURIComponent(url)}&count=${userCount * requestCount}`);
        if (!response.ok) throw new Error('Network response was not ok');

        const data = await response.json();
        if (!Array.isArray(data.latencies)) throw new Error('Invalid response format: latencies are missing or not an array');

        const latenciesInSeconds = data.latencies.map(latency => (latency / 1000).toFixed(3));
        displayResults(data, latenciesInSeconds);
        createChart(ctx, latenciesInSeconds);
    } catch (error) {
        console.error('Error fetching latency data:', error);
        document.getElementById('result').innerHTML = `<p class="error">Error: ${error.message}</p>`;
    } finally {
        document.getElementById('loader').style.display = 'none';
    }
}

// Part 3: Display Results
function displayResults(data, latenciesInSeconds) {
    document.getElementById('result').innerHTML = `
        <h3>Total Latency: ${data.totalLatency} ms (${(data.totalLatency / 1000).toFixed(3)} s)</h3>
        <h4>Individual Latencies:</h4>
        <p>Below are the latencies recorded for each request:</p>
        <ul>
            ${data.latencies.map(latency => `<li>${latency} ms (${(latency / 1000).toFixed(3)} s)</li>`).join('')}
        </ul>
        <div id="interpretation">
            <h4>Interpretation of Results:</h4>
            <p>The total latency of <strong>${data.totalLatency} ms (${(data.totalLatency / 1000).toFixed(3)} s)</strong> indicates the total time taken for all requests to complete. A lower total latency generally means a more responsive application. If the total latency is significantly high, it may indicate potential issues with the server or network.</p>
            <p>Individual latencies can vary based on several factors, including server load, network conditions, and the complexity of the requests made. If you notice consistently high latencies for specific requests, it may be worth investigating further.</p>
            <p>Generally, a latency below 100 ms is considered excellent, while latencies between 100 ms and 300 ms are acceptable for most applications. Latencies above 300 ms may lead to a noticeable delay in user experience.</p>
        </div>
    `;
}

// Part 4: Chart Creation
function createChart(ctx, latenciesInSeconds) {
    const labels = latenciesInSeconds.map((_, index) => `Request ${index + 1}`);
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Latency (seconds)',
                data: latenciesInSeconds,
                borderColor: 'rgba(75, 192, 192, 1)',
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderWidth: 2,
                fill: true
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Latency (s)'
                    }
                },
                x: {
                    title: {
                        display: true,
                        text: 'Requests'
                    }
                }
            }
        }
    });
}


function displayResults(data, latenciesInSeconds) {
    const totalLatencySeconds = (data.totalLatency / 1000).toFixed(3);
    document.getElementById('result').innerHTML = `
        <h3>Total Latency: ${data.totalLatency} ms (${totalLatencySeconds} s)</h3>
        <h4>Individual Latencies:</h4>
        <p>Below are the latencies recorded for each request:</p>
        <ul>
            ${data.latencies.map(latency => `<li>${latency} ms (${(latency / 1000).toFixed(3)} s)</li>`).join('')}
        </ul>
        <div id="interpretation">
            <h4>Interpretation of Results:</h4>
            <p>The total latency of <strong>${data.totalLatency} ms (${totalLatencySeconds} s)</strong> indicates the total time taken for all requests to complete. A lower total latency generally means a more responsive application. If the total latency is significantly high, it may indicate potential issues with the server or network.</p>
            <p>Individual latencies can vary based on several factors, including server load, network conditions, and the complexity of the requests made. If you notice consistently high latencies for specific requests, it may be worth investigating further.</p>
            <p>Generally, a latency below 100 ms is considered excellent, while latencies between 100 ms and 300 ms are acceptable for most applications. Latencies above 300 ms may lead to a noticeable delay in user experience.</p>
        </div>
    `;
}


// scraping latency

document.getElementById('testButtonScraper').addEventListener('click', async () => {
    const scraperUrl = document.getElementById('scraperUrl').value;
    const scraperRequestCount = parseInt(document.getElementById('scraperRequestCount').value);
    const resultDiv = document.getElementById('result');
    const latencyChartCanvas = document.getElementById('latencyChartScraper');
    const ctx = latencyChartCanvas.getContext('2d');

    // Clear previous results
    resultDiv.innerHTML = '';
    ctx.clearRect(0, 0, latencyChartCanvas.width, latencyChartCanvas.height);

    if (!scraperUrl) {
        resultDiv.innerHTML = 'Please enter a valid scraper URL.';
        return;
    }

    if (isNaN(scraperRequestCount) || scraperRequestCount < 1 || scraperRequestCount > 10) {
        resultDiv.innerHTML = 'Please enter a valid number of requests (1-10).';
        return;
    }

    // Show loader
    document.getElementById('loader').style.display = 'block';

    const latencies = [];
    
    for (let i = 0; i < scraperRequestCount; i++) {
        const startTime = performance.now();
        try {
            await fetch(scraperUrl);
            const endTime = performance.now();
            latencies.push(endTime - startTime);
        } catch (error) {
            console.error('Error fetching the URL:', error);
            latencies.push(null); // Push null if there's an error
        }
    }

    // Hide loader
    document.getElementById('loader').style.display = 'none';

    // Filter out null values and calculate average latency
    const validLatencies = latencies.filter(latency => latency !== null);
    const averageLatency = validLatencies.length ? (validLatencies.reduce((a, b) => a + b) / validLatencies.length).toFixed(2) : null;

    // Display results
    if (averageLatency !== null) {
        resultDiv.innerHTML = `Average Latency: ${averageLatency} ms`;
    } else {
        resultDiv.innerHTML = 'All requests failed.';
    }

    // Render 3D chart
    const scraperLatencyChart = new Chart(ctx, {
        type: 'bar', // Use 'bar' for a 3D effect
        data: {
            labels: Array.from({ length: scraperRequestCount }, (_, i) => `Request ${i + 1}`),
            datasets: [{
                label: 'Latency (ms)',
                data: latencies,
                backgroundColor: 'rgba(75, 192, 192, 0.5)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1,
                // Optional: Add depth to the bars
                depth: 10,
            }]
        },
        options: {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Latency (ms)'
                    }
                }
            },
            plugins: {
                legend: {
                    display: true,
                },
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return `Latency: ${tooltipItem.raw} ms`;
                        }
                    }
                }
            },
            // Enable 3D options
            elements: {
                bar: {
                    borderWidth: 2,
                    borderRadius: 5,
                }
            }
        }
    });

    // Interpretation of results
    if (averageLatency !== null) {
        if (averageLatency < 100) {
            resultDiv.innerHTML += '<br>Interpretation: Excellent latency.';
        } else if (averageLatency < 300) {
            resultDiv.innerHTML += '<br>Interpretation: Good latency.';
        } else if (averageLatency < 500) {
            resultDiv.innerHTML += '<br>Interpretation: Fair latency, consider optimization.';
        } else {
            resultDiv.innerHTML += '<br>Interpretation: Poor latency, optimization is needed.';
        }
    }
});